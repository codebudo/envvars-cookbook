
# Sets the environment variables permanently
template "/etc/profile.d/chef-environment-vars.sh" do
  source "chef-environment-vars.sh.erb"
  variables :vars => node['envvars']['environment']
end

# Sets the same vars for the rest of this chef run
node['envvars']['environment'].each do |k, v|
  ENV[k] = v
end
