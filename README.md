envvars Cookbook
================
Persistently set environment variables on a linux system by adding a file to the /etc/profile.d directory. This script is `source`d at login so all users on the system will have these environment variables.


Attributes
----------
TODO: List your cookbook attributes here.

e.g.
#### envvars::default
<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['envvars']['environment']</tt></td>
    <td>Array</td>
    <td>An array of key->value pairs</td>
    <td><tt>[]</tt></td>
  </tr>
</table>

Usage
-----
#### envvars::default

Just include `envvars` in your node's `run_list` and set your environment variables in the attribute described above.

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[envvars]"
  ]
}
```

Contributing
------------

1. Fork the repository on Github
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Pull Request using Github

License and Authors
-------------------
Authors: TODO: S. Mikkel Wilson
